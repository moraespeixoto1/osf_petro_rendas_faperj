library(tidyverse)
library(readxl)
library(haven)
library(deflateBR)

#####
###
# adicionar ibge6 e ibge7
ibge <- read_sav("banco_de_dados/bancos_base/BANCO_DEMOGRAFICO.sav") %>%
  select(IBGE6, IBGE7, TSE, NOME_MUNICIPIO, UF,ESTADO) %>%
  janitor::clean_names()

ibge$nome_municipio <- str_to_upper(ibge$nome_municipio)
ibge$nome_municipio <- abjutils::rm_accent(ibge$nome_municipio)
ibge$estado <- str_to_upper(ibge$estado)
ibge$estado <- abjutils::rm_accent(ibge$estado)

#####
###
# Adicionar dados do finbra (gasto municipal com saude e educacao) ao banco 2008-2022

# finbra 2022

# AS DESPESAS DE 2022 AINDA NAO SAIRAM. O BANCO DISPONIBILIZADO NO DIA 14/01/23 SO TEM 1 MUNICIPIO

# finbra 2021
finbra_2021 <- read_delim("banco_de_dados/bancos_base/finbra_despesa_2021.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2021)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2021 <- filter(finbra_2021, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2021 <-  spread(finbra_2021, Conta, Valor) %>%
  rename("gasto_saude_2021" = "10 - Saúde",
         "gasto_educacao_2021"  = "12 - Educação",
         "pop_2021_siconfi" = "População")

# finbra 2020
finbra_2020 <- read_delim("banco_de_dados/bancos_base/finbra_2020.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2020)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2020 <- filter(finbra_2020, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2020 <-  spread(finbra_2020, Conta, Valor) %>%
  rename("gasto_saude_2020" = "10 - Saúde",
         "gasto_educacao_2020"  = "12 - Educação",
         "pop_2020_siconfi" = "População")

# finbra 2019
finbra_2019 <- read_delim("banco_de_dados/bancos_base/finbra_2019.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2019)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2019 <- filter(finbra_2019, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2019 <-  spread(finbra_2019, Conta, Valor) %>%
  rename("gasto_saude_2019" = "10 - Saúde",
         "gasto_educacao_2019"  = "12 - Educação",
         "pop_2019_siconfi" = "População")

# finbra 2018
finbra_2018 <- read_delim("banco_de_dados/bancos_base/finbra_2018.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2018)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2018 <- filter(finbra_2018, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2018 <-  spread(finbra_2018, Conta, Valor) %>%
  rename("gasto_saude_2018" = "10 - Saúde",
         "gasto_educacao_2018"  = "12 - Educação",
         "pop_2018_siconfi" = "População")

# finbra 2017
finbra_2017 <- read_delim("banco_de_dados/bancos_base/finbra_2017.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2017)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2017 <- filter(finbra_2017, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2017 <-  spread(finbra_2017, Conta, Valor) %>%
  rename("gasto_saude_2017" = "10 - Saúde",
         "gasto_educacao_2017"  = "12 - Educação",
         "pop_2017_siconfi" = "População")

# finbra 2016
finbra_2016 <- read_delim("banco_de_dados/bancos_base/finbra_2016.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2016)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2016 <- filter(finbra_2016, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2016 <-  spread(finbra_2016, Conta, Valor) %>%
  rename("gasto_saude_2016" = "10 - Saúde",
         "gasto_educacao_2016"  = "12 - Educação",
         "pop_2016_siconfi" = "População")

# finbra 2015
finbra_2015 <- read_delim("banco_de_dados/bancos_base/finbra_2015.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2015)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2015 <- filter(finbra_2015, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2015 <-  spread(finbra_2015, Conta, Valor) %>%
  rename("gasto_saude_2015" = "10 - Saúde",
         "gasto_educacao_2015"  = "12 - Educação",
         "pop_2015_siconfi" = "População")

# finbra 2014
finbra_2014 <- read_delim("banco_de_dados/bancos_base/finbra_2014.csv",
                          delim = ";", escape_double = FALSE, col_names = FALSE,
                          locale = locale(decimal_mark = ",", grouping_mark = "",
                                          encoding = "ISO-8859-1"), trim_ws = TRUE,
                          skip = 4)
names(finbra_2014)[1:8] <- c("Instituição","CodIBGE","UF","População","Coluna","Conta","Identificador da Conta","Valor")

finbra_2014 <- filter(finbra_2014, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2014 <-  spread(finbra_2014, Conta, Valor) %>%
  rename("gasto_saude_2014" = "10 - Saúde",
         "gasto_educacao_2014"  = "12 - Educação",
         "pop_2014_siconfi" = "População")

# finbra 2013
finbra_2013 <- read_csv("banco_de_dados/bancos_base/finbra_2013.csv",
                        locale = locale(decimal_mark = ",", encoding = "ISO-8859-1"))

finbra_2013 <- filter(finbra_2013, Conta == "10 - Saúde" | Conta == "12 - Educação", Coluna == "Despesas Empenhadas") %>%
  select(CodIBGE, Conta, Valor, População)

finbra_2013 <-  spread(finbra_2013, Conta, Valor) %>%
  rename("gasto_saude_2013" = "10 - Saúde",
         "gasto_educacao_2013"  = "12 - Educação",
         "pop_2013_siconfi" = "População")

# finbra 2012
finbra_2012 <- read_excel("banco_de_dados/bancos_base/finbra_2012.xlsx") %>%
  select(UF, MUNICIPIO, Saúde, Educação, Populacao) %>%
  rename("gasto_saude_2012"="Saúde",
         "gasto_educacao_2012"="Educação",
         "pop_2012_siconfi" = "Populacao")

finbra_2012$MUNICIPIO <- str_to_upper(finbra_2012$MUNICIPIO)
finbra_2012$MUNICIPIO <- abjutils::rm_accent(finbra_2012$MUNICIPIO)

# finbra 2011
finbra_2011 <- read_excel("banco_de_dados/bancos_base/finbra_2011.xlsx") %>%
  select(UF, MUNICIPIO, Saúde, Educação, Populacao) %>%
  rename("gasto_saude_2011"="Saúde",
         "gasto_educacao_2011"="Educação",
         "pop_2011_siconfi" = "Populacao")

finbra_2011$MUNICIPIO <- str_to_upper(finbra_2011$MUNICIPIO)
finbra_2011$MUNICIPIO <- abjutils::rm_accent(finbra_2011$MUNICIPIO)

# finbra 2010
finbra_2010 <- read_excel("banco_de_dados/bancos_base/finbra_2010.xlsx") %>%
  select(UF, MUNICIPIO, Saúde, Educação, Populacao) %>%
  rename("gasto_saude_2010"="Saúde",
         "gasto_educacao_2010"="Educação",
         "pop_2010_siconfi" = "Populacao")

finbra_2010$MUNICIPIO <- str_to_upper(finbra_2010$MUNICIPIO)
finbra_2010$MUNICIPIO <- abjutils::rm_accent(finbra_2010$MUNICIPIO)

# finbra 2009
finbra_2009 <- read_excel("banco_de_dados/bancos_base/finbra_2009.xlsx") %>%
  select(UF, Município, Saúde, Educação, Populacao) %>%
  rename("gasto_saude_2009"="Saúde",
         "gasto_educacao_2009"="Educação",
         "MUNICIPIO"="Município",
         "pop_2009_siconfi" = "Populacao")

finbra_2009$MUNICIPIO <- str_to_upper(finbra_2009$MUNICIPIO)
finbra_2009$MUNICIPIO <- abjutils::rm_accent(finbra_2009$MUNICIPIO)

# finbra 2008
finbra_2008 <- read_excel("banco_de_dados/bancos_base/finbra_2008.xlsx") %>%
  select(UF, Município, Saúde, Educação, Populacao) %>%
  rename("gasto_saude_2008"="Saúde",
         "gasto_educacao_2008"="Educação",
         "MUNICIPIO"="Município",
         "pop_2008_siconfi" = "Populacao")

finbra_2008$MUNICIPIO <- str_to_upper(finbra_2008$MUNICIPIO)
finbra_2008$MUNICIPIO <- abjutils::rm_accent(finbra_2008$MUNICIPIO)

# unindo os finbra 2008-2012 (gasto com saude e educacao)
finbra_2008_2012 <- full_join(finbra_2008, finbra_2009, by=c("UF","MUNICIPIO"), na_match="never")
finbra_2008_2012 <- full_join(finbra_2008_2012, finbra_2010, by=c("UF","MUNICIPIO"), na_match="never")
finbra_2008_2012 <- full_join(finbra_2008_2012, finbra_2011, by=c("UF","MUNICIPIO"), na_match="never")
finbra_2008_2012 <- full_join(finbra_2008_2012, finbra_2012, by=c("UF","MUNICIPIO"), na_match="never")

# adicionou ibge7 aos bancos do finbra 2008-2012
ibge7 <- ibge %>%
  select(ibge7, uf, nome_municipio)

finbra_2008_2012 <- left_join(ibge7, finbra_2008_2012, by=c("uf"="UF","nome_municipio"="MUNICIPIO"), na_match="never")

# unindo os finbra (gasto com saude e educacao) 2008-2020
finbra <- full_join(finbra_2013, finbra_2014, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2015, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2016, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2017, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2018, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2019, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2020, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2021, by="CodIBGE", na_match="never")
finbra <- full_join(finbra, finbra_2008_2012, by=c("CodIBGE"="ibge7"), na_match="never") %>%
  rename("ibge7"="CodIBGE") %>%
  select(!c(uf, nome_municipio))

#####
###
# Deflacionar as despesas com saude e educacao 2008-2020
finbra$gasto_saude_2008_ipca <- deflate(finbra$gasto_saude_2008, as.Date("2008-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2009_ipca <- deflate(finbra$gasto_saude_2009, as.Date("2009-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2010_ipca <- deflate(finbra$gasto_saude_2010, as.Date("2010-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2011_ipca <- deflate(finbra$gasto_saude_2011, as.Date("2011-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2012_ipca <- deflate(finbra$gasto_saude_2012, as.Date("2012-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2013_ipca <- deflate(finbra$gasto_saude_2013, as.Date("2013-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2014_ipca <- deflate(finbra$gasto_saude_2014, as.Date("2014-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2015_ipca <- deflate(finbra$gasto_saude_2015, as.Date("2015-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2016_ipca <- deflate(finbra$gasto_saude_2016, as.Date("2016-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2017_ipca <- deflate(finbra$gasto_saude_2017, as.Date("2017-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2018_ipca <- deflate(finbra$gasto_saude_2018, as.Date("2018-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2019_ipca <- deflate(finbra$gasto_saude_2019, as.Date("2019-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2020_ipca <- deflate(finbra$gasto_saude_2020, as.Date("2020-01-01"),"01/2022", "ipca")
finbra$gasto_saude_2021_ipca <- deflate(finbra$gasto_saude_2021, as.Date("2021-01-01"),"01/2022", "ipca")

finbra$gasto_educacao_2008_ipca <- deflate(finbra$gasto_educacao_2008, as.Date("2008-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2009_ipca <- deflate(finbra$gasto_educacao_2009, as.Date("2009-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2010_ipca <- deflate(finbra$gasto_educacao_2010, as.Date("2010-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2011_ipca <- deflate(finbra$gasto_educacao_2011, as.Date("2011-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2012_ipca <- deflate(finbra$gasto_educacao_2012, as.Date("2012-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2013_ipca <- deflate(finbra$gasto_educacao_2013, as.Date("2013-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2014_ipca <- deflate(finbra$gasto_educacao_2014, as.Date("2014-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2015_ipca <- deflate(finbra$gasto_educacao_2015, as.Date("2015-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2016_ipca <- deflate(finbra$gasto_educacao_2016, as.Date("2016-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2017_ipca <- deflate(finbra$gasto_educacao_2017, as.Date("2017-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2018_ipca <- deflate(finbra$gasto_educacao_2018, as.Date("2018-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2019_ipca <- deflate(finbra$gasto_educacao_2019, as.Date("2019-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2020_ipca <- deflate(finbra$gasto_educacao_2020, as.Date("2020-01-01"),"01/2022", "ipca")
finbra$gasto_educacao_2021_ipca <- deflate(finbra$gasto_educacao_2021, as.Date("2021-01-01"),"01/2022", "ipca")

# salvar banco finalizado
write.csv(finbra, "banco_de_dados/bancos_base/despesas_saude_educacao_2008_2021.csv", fileEncoding = "latin1")
