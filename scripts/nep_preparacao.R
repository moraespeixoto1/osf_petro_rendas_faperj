library(tidyverse)


load("~/osf_petro_rendas_faperj/bancos_base/competicao_eleitoral/nep_municipal.Rda")

nep_mun_long <- nep_municipal %>% 
  ungroup() %>% 
  filter(NR_TURNO == 1,
         DS_ELEICAO %in% c("ELEICOES 2000", "ELEICOES 2004", "ELEIÇÕES 2008", "Eleições Municipais 2016",
  "Eleições Municipais 2020", "Eleições Municipais 22020 - AP", "ELEIÇÃO MUNICIPAL 2012" )) %>% 
  rename(ano = "ANO_ELEICAO") %>% 
  select(1, 5, 9, 10) %>% 
  mutate(DS_CARGO = toupper(DS_CARGO),
         id_municipio_tse = as.numeric(SG_UE)) %>% 
  unique() %>% # elimina 1 unico caso repetido
  pivot_wider(values_from = "nep", names_from = "DS_CARGO", names_prefix = "Nep_" )



codigos <- read_csv("~/osf_petro_rendas_faperj/bancos_base/codigos.csv")

codigos <- codigos  %>% 
  select(1, 3, 21, 22)


nep_mun_long <- full_join(nep_mun_long, codigos,
                          by = "id_municipio_tse")

save(nep_mun_long, file = "~/osf_petro_rendas_faperj/bancos_base/competicao_eleitoral/nep_mun_long.Rda")



