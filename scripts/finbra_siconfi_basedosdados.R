# Pacotes utilizados
library(tidyverse)
library(factoextra)
library(outliers)
library(readr)
library(janitor)
library(ggthemes)
options(scipen = 99)

#### Banco de despesas ####

# Consulta SQL basedosdados (SELECT id_municipio,ano, estagio, conta, estagio_bd, id_conta_bd, conta_bd, valor FROM `basedosdados.br_me_siconfi.municipio_despesas_funcao` WHERE ano >= 2000 AND ano <= 2007)

despesas_2015_2022 <- read_csv("bancos_base/FINBRA_BASE_DOS_DADOS/DESPESAS_FINBRA_2015_2002.csv", 
                                      col_types = cols(valor = col_number())) %>% 
  janitor::clean_names()


despesas_2008_2014 <- read_csv("bancos_base/FINBRA_BASE_DOS_DADOS/DESPESAS_FINBRA_2008_2014.csv", 
                               col_types = cols(valor = col_number())) %>% 
  janitor::clean_names()


despesas_2000_2007 <- read_csv("bancos_base/FINBRA_BASE_DOS_DADOS/DESPESAS_FINBRA_2000_2007.csv", 
                               col_types = cols(valor = col_number())) %>% 
  janitor::clean_names()


despesas_siconfi <- rbind(despesas_2008_2014, 
                          despesas_2015_2022,
                          despesas_2000_2007) 

save(despesas_siconfi, file = "~/osf_petro_rendas_faperj/bancos_base/FINBRA_BASE_DOS_DADOS/despesas_siconfi.Rda")

## Despesas wide ##

despesas_wide <- despesas_siconfi %>% 
  filter(estagio_bd == "Despesas Empenhadas",
         conta_bd %in% c("Educação", "Saúde", "Administração", "Assistência Social", 
            "Despesas Exceto Intraorçamentárias")) %>% 
  select(1, 2, 7, 8) %>% 
  pivot_wider(names_from = "conta_bd", values_from = "valor", values_fn = mean ) %>% 
  clean_names() %>% 
  mutate(perc_educacao = educacao/despesas_exceto_intraorcamentarias*100,
         perc_saude = saude/despesas_exceto_intraorcamentarias*100,
         perc_administracao = administracao/despesas_exceto_intraorcamentarias*100,
         perc_assistencia = assistencia_social/despesas_exceto_intraorcamentarias*100) %>% 
  group_by(id_municipio) %>% 
  mutate(z_perc_educacao = scale(perc_educacao),
         z_perc_saude = scale(perc_saude),
         z_perc_administracao = scale(perc_administracao),
         z_perc_assistencia = scale(perc_assistencia))


save(despesas_wide, file = "~/osf_petro_rendas_faperj/bancos_base/FINBRA_BASE_DOS_DADOS/despesas_wide.Rda")

load("~/osf_petro_rendas_faperj/bancos_base/FINBRA_BASE_DOS_DADOS/despesas_wide.Rda")

# Testes visuais das despesas (e os respectivos gastos em percentual da despesa total empenhada)
 ggplot(despesas_wide)+
   aes(x= as_factor(ano), y = z_perc_assistencia)+
   geom_boxplot()+
   theme_minimal()
  
  
 ggplot(despesas_wide)+
   aes(x= perc_educacao, y = perc_saude)+
   geom_point()+
   theme_minimal()+
   facet_wrap(~ano)
 
 
#### Banco de receitas ####

 # Consulta receitas SQL basedosdados (SELECT ano, sigla_uf, id_municipio, estagio, conta, estagio_bd, id_conta_bd, conta_bd, valor FROM `basedosdados.br_me_siconfi.municipio_receitas_orcamentarias` WHERE ano <= 2007 AND ano >= 2000  )
 
receitas_2015_2022 <- read_csv("bancos_base/FINBRA_BASE_DOS_DADOS/RECEITAS_FINBRA_2015_2022.csv", 
                               col_types = cols(valor = col_number()))

receitas_2008_2014 <- read_csv("bancos_base/FINBRA_BASE_DOS_DADOS/RECEITAS_FINBRA_2008_2014.csv", 
                                      col_types = cols(valor = col_number()))

receitas_2000_2007 <- read_csv("bancos_base/FINBRA_BASE_DOS_DADOS/RECEITAS_FINBRA_2000_2007.csv", 
                               col_types = cols(valor = col_number()))

receitas_siconfi <- rbind(receitas_2008_2014, 
                          receitas_2015_2022,
                          receitas_2000_2007)

save(receitas_siconfi, file = "~/osf_petro_rendas_faperj/bancos_base/FINBRA_BASE_DOS_DADOS/receitas_siconfi.Rda")

load("~/osf_petro_rendas_faperj/bancos_base/FINBRA_BASE_DOS_DADOS/receitas_siconfi.Rda")


royalties <- receitas_siconfi %>% 
  filter(str_detect(conta_bd, regex("royalties", ignore_case = TRUE))) %>% 
  group_by(id_municipio, ano) %>% 
  summarise(valor_receitas_royalties = sum(valor)) %>% 
  ungroup()

receitas_orcamentarias <- receitas_siconfi %>% 
  filter(conta_bd == "Receitas Orçamentárias") %>% 
  group_by(id_municipio, ano) %>% 
  summarise(valor_receitas_orcamentarias = sum(valor)) %>% 
  ungroup()

receitas_siconfi_final <- full_join(receitas_orcamentarias, royalties)

receitas_siconfi_final <- receitas_siconfi_final %>% 
  mutate(perc_royalties = valor_receitas_royalties/valor_receitas_orcamentarias*100)

# Exame visual receitas 
receitas_siconfi_final %>% 
  filter(perc_royalties > 0) %>% 
ggplot() +
  aes(x = as_factor(ano), y = perc_royalties)+
  geom_boxplot()


#### Unificacao banco de despesas e receitas ####

despesas_receitas <- full_join(despesas_wide, receitas_siconfi_final,
                               by = c("id_municipio", "ano"))


ggplot(despesas_receitas)+
  aes(x = log(despesas_exceto_intraorcamentarias), y = log(valor_receitas_orcamentarias))+
  geom_point()+
  facet_wrap(~ano)


save(despesas_receitas, file = "~/osf_petro_rendas_faperj/bancos_base/FINBRA_BASE_DOS_DADOS/despesas_receitas.Rda")


load(file = "~/osf_petro_rendas_faperj/bancos_base/FINBRA_BASE_DOS_DADOS/despesas_receitas.Rda")




