# Desenho de pesquisa

 O desenho de pesquisa trata do plano ou estratégia desenvolvida pelos pesquisadores para conduzir a investigação aqui proposta. Ele inclui detalhes sobre como a pesquisa foi realizada, os métodos utilizados, como os dados foram coletados e analisados, e quais são as questões de pesquisa que estão sendo investigadas. O desenho de pesquisa é importante porque ajuda a garantir que a pesquisa seja conduzida de maneira sistemática e rigorosa, permitindo que os pesquisadores obtenham resultados mais confiáveis e com maior grau de precisão.
 
 Neste trabalho foram utilizados desenhos de pesquisas distintos para cada problema abordado em diferentes capítulos. Nos estudos comparativos longitudinais foram utilizados. Os desenhos combinam forma de coleta de dados, usos do tempo e relações de associações entre fenômenos. 
 
 Nos capítulos sobre gastos sociais e no que versou sobre competição eleitoral foram utilizados o desenho de pesquisa com dados em painel, que trata-se de um tipo de estudo longitudinal que envolve a coleta de dados repetidos ao longo do tempo em uma amostra fixa de unidades de análise - neste caso, os municípios. Esse tipo de pesquisa é chamado de "dados em painel" porque os dados são organizados em um formato de tabela com as unidades de análise nas linhas e as medidas repetidas ao longo do tempo nas colunas, o que permite que se examinem mudanças ao longo do tempo e avaliem o impacto das variáveis independentes nas variáveis dependentes, controlando fatores que permanecem constantes ao longo do tempo. Além disso, esse tipo de estudo pode ajudar a reduzir o viés causado por diferenças individuais entre os participantes, pois cada indivíduo serve como seu próprio controle. 

 Há tambem um capítulo sobre reeleição nos municípios que utiliza o desenho com dados transversais (**Cross-Sectional Observational Studies**), pois foram tratados dados considerados de um único ponto do tempo - em realidade, o ponto de cada município foi a média do ciclo eleitoral de quatro anos. Nesse tipo de desenho de pesquisa, os dados são coletados em um momento específico e as variáveis independentes e dependentes são medidas simultaneamente. 
 
 O desenho de pesquisa de *Estudos de Casos* foi utilizado na capítulo sobre Eleições de 2020 em Campos dos Goytacazes. Apesar de focar as análises em uma única eleição, o contexto histórico das lutas políticas entre as famílias contemplou ciclos eleitorais desde os anos 2000. De acordo com @kellstedtFundamentalsPoliticalScience2018, os estudos de caso são uma abordagem de pesquisa qualitativa que se concentra em examinar um fenômeno em profundidade e em seu contexto natural. Algumas das características dos estudos de caso incluem:

1. Foco em um caso específico: os estudos de caso se concentram em um único caso ou em um número limitado de casos, permitindo uma análise detalhada e aprofundada.

2. Contextualização: os estudos de caso buscam entender o fenômeno estudado dentro do contexto mais amplo no qual ele ocorre, levando em consideração fatores sociais, culturais, políticos e históricos.

3. Abordagem holística: os estudos de caso buscam entender o fenômeno estudado como um todo, levando em consideração múltiplas perspectivas e variáveis.

4. Coleta de dados múltiplos: os estudos de caso utilizam várias fontes de dados para obter uma compreensão completa do fenômeno estudado, incluindo entrevistas, observações, documentos e registros.

5. Análise indutiva: os estudos de caso utilizam uma abordagem indutiva para análise dos dados coletados, permitindo que as conclusões surjam a partir dos próprios dados.

6. Ênfase na validade interna: os estudos de caso buscam garantir a validade interna através da triangulação dos dados e da verificação cruzada das informações coletadas.

7. Generalização limitada: embora possam fornecer insights valiosos sobre o fenômeno estudado, os resultados dos estudos de caso não podem ser generalizados para outras situações ou contextos semelhantes sem uma análise cuidadosa.
 

## Metodologia e estratégias de ação

 Para conseguir alcançar os objetivos traçados, este projeto terá o respaldo metodológico de métodos tanto quantitativos quanto de qualitativos. Esta sessão tem como função demonstrar os procedimentos que serão realizados ao longo desta empreitada. 


### Técnica de Investigação

 A dinâmica das interações entre das indenizações petrolíferas, instituições políticas, financiamento de políticas sociais e as condições socioeconômicas nos municípios brasileiros serão investigadas por meio de análises de regressão linear múltipla e modelos estruturais, quando necessários correções para autocorrelações espaciais serão também utilizados. 

### Unidades de análise

 As unidades de observação serão todos municípios brasileiros com informações disponíveis nos censos demográficos de 2000, 2010 e 2020. Para o caso das variáveis políticas, serão considerados os quatro últimos governos (2000-2004; 2008- 2012; 2012 - 2016; 2016 – 2020).

### Por que comparar municípios?

 Desenhos de pesquisas que privilegiam análises de países são muito comuns em trabalhos de política comparada. Existe uma série de fatores que justificam a escolha destas unidades de análise, tais como: unidades geográficas razoavelmente bem delimitadas, facilidade de obtenção de informações (dados), permitem observar variações cultural, religiosa, histórica e institucional etc. Por outro lado, análises que observam um único país podem ser consideras análises de casos, ainda que trate de muitas observações, seja longitudinal ou em um mesmo ponto no tempo (cross-section).  Neste último caso, existem também vantagens metodológicas, como manter constantes fatores institucionais e permitir variação de outras características desejadas. Esta é uma das características, portanto, que justificam metodologicamente a escolha dos municípios brasileiros como unidades observacionais. 
  
 São aproximadamente 5.570 municípios em que se constata uma série de instituições políticas bastante semelhantes, tal como o sistema eleitoral, assim como uma enorme variação de outras características como desigualdade, indicadores de riqueza, educação, competição eleitoral, equipamentos de comunicação (emissoras de rádio e TV) e etc. Em resumo, análises dos sistemas municipais podem ser classificadas com estudos de caso, dado que tratam apenas do Brasil, entretanto, a multiplicidade de observações confere status de análises quantitativas de muitos casos voltadas para relação entre variáveis.

 Há ainda outras justificativas de ordem acadêmica, a saber, a escassez de análises dos sistemas políticos locais. Não obstante a grande evolução da ciência política brasileira nas análises do sistema político nacional, pouca atenção tem sido dada aos sistemas locais. Diagnósticos sobre o sistema político brasileiro que negligenciam a política municipal correm sérios riscos de cometer falácias de desagregação (ou da divisão, aquela que toma as partes pelo todo). Santos e Guimarães (mimeo), ao se lançarem na empreitada de analisar o sistema partidário brasileiro, no intuito de desconstruir a concepção de “partidos de aluguel”, afirmam que grande parte dos equívocos analíticos e preconceitos contra pequenos partidos decorrem exatamente do privilégio e exclusividade que normalmente os analistas conferem à Câmara dos Deputados. 
 

>>> “[O] severo julgamento [dos pequenos partidos] das análises correntes decorre da ilusão de ótica proporcionada pela exclusividade com que as eleições para a Câmara de Deputados tem sido beneficiadas nas investigações. [...] No privilegiado nível nacional de competição, o número de agremiações que não conseguem obter cadeiras ou as obtém em número irrisório transmite a impressão de que representam legendas estranhas à função legislativa em si mesma, sendo, possivelmente, outros os seus objetivos materiais.” (Santos e Guimarães, mimeo, pp: 12)


 Como já havia apontado Nicolau (1996), ao analisar as motivações para a formação de coligações, o fato de ser um grande partido na Câmara dos Deputados não significa que assim o seja em todas as unidades da federação. O mesmo havia apontado Lima Jr. (1983) acerca dos subsistemas estaduais, cada qual com sua própria lógica e respondendo a estímulos institucionais diferentes, tal como as magnitudes dos distritos. Toda e qualquer análise acerca da distribuição do sistema partidário nacional, portanto, deveria partir de análises dos Estados exatamente pelo efeito de agregação provocado pelo sistema eleitoral que tem nestes as delimitações dos distritos eleitorais.  

 Santos e Guimarães (no prelo) descem mais ainda no nível de análise e transportam esse argumento para os sistemas locais, chegam mesmo a concluir, ao contrário do que se apregoa nos noticiários nacionais, um robusto crescimento das pequenas legendas nos municípios brasileiros e a absoluta falta de critério para condenar a priori estes partidos como ilegítimos (partidos de aluguéis) no cenário onde atuam:

 >>> “O pessimismo de semelhantes prognósticos é bastante questionável, em particular o do presumido contagio institucional, segundo o qual os partidos ditos “de aluguel” representam estadualmente e municipalmente o mesmo que representariam a nível nacional, isto é, praticamente nada.” (Santos e Guimarães, no prelo, pp: 13-14)

Torna-se patente, pelos motivos acima expostos, a necessidade de se avaliar os sistemas municipais com instrumentos analíticos capazes de dar conta da importância que realmente possuem, assim como das consequências que podem ter para o sistema nacional.

 
###  Estratégias de ação para a realização do trabalho
 

#### Investigação teórica

Primeiro passo desta empreitada é a realização da uma incursão na literatura do tema central desta pesquisa, a saber, as consequencias socioeconômicas e políticas da dependencia dos recursos oriundos de rendas petrolíferas no sistema subnacional. Portanto, a revisão bibliográfica tem o objetivo de mapear as principais produções nacionais acerca do poder local, administração pública e condições socioeconômicas dos municípios brasileiros, políticas sociais e royalties de petróleo. Breve análise sobre a produção internacional de política comparada, principalmente, aquela que versa sobre os testes da teoria da modernização.

O capítulo seguinte será dedicado exclusivamente ao mapeamento desta literatura que se debruçou sobre os temas aqui tratados. Apesar de não se propor exautivo, o objetivo foi levantar a produção acadêmica de forma mais abrangente possível.  

#### Operacionalização das variáveis dependentes

Construção de indicadores que possibilitem a aferição das principais características das administrações municipais tais como: 

 - Incremento orçamentário total
 - Captação de recursos de outras esferas governamentais de origem negociada
 - Recebimento de rendas petrolíferas (royalties e participações especiais)
 
  
  Construção de Indicadores que possibilitem aferição dos padrões de gastos sociais realizados pelos municípios, tais como:
 - Gastos com assistência social
 - Gastos com saúde
 - Gastos com educação 

Construção de Indicadores que possibilitem aferição das características da política eleitoral tais como:
Número efetivo de partidos e fracionalização eleitoral (proporcional e majoritário):

 - Número de candidatos/vagas para vereadores
 - Número efetivos de partidos
 - Volatilidade
 - Participação eleitoral (comparecimento, votos brancos e nulos)
 
 
 ####  Operacionalização dos fatores explicativos 
 

Utilização de indicadores que caracterizam os aspectos socioeconômicos dos municípios em três pontos no tempo aferidos pelos censos demográficos do IBGE (1990, 2000 e 2010)

 Indicadores que afiram as características educacionais: 

 - Taxa de alfabetização de pessoas com 15 anos ou mais
 - Média de escolarização em anos de estudo dos chefes de domicílio
 - Média de escolarização em anos de estudo das mulheres economicamente ativas
 - Número de matrículas do ensino superior (em proporção à população)
 - Número de matrículas do ensino fundamental
 
 Indicadores que afiram características econômicas:  

 - PIB per capta; rendimento médio dos chefes de domicílio
 - Percentual de receitas oriundas da indústria
 - Percentual de receitas oriundas de prestação de serviços
 - Percentual de receitas oriundas da agropecuária
 - Desigualdade de renda

 Indicadores que afiram as características de saúde:

 - Número de leitos, enfermeiros e médicos por 1.000 habitantes
 - Existência de hospitais públicos
 

 Construção de Indicadores que possibilitem aferição das receitas advindas das indenizações petrolíferas repassadas pelos municípios brasileiros tais como:
 
 - Cota parte Royalties, cota parte compensação financeira pela a produção de petróleo
 - Cota parte Royalties, pelo o excedente da produção do petróleo
 - Cota parte Royalties, pela a participação especial
 - Cota parte, Fundo Especial do Petróleo (FEP)
 - Cota parte Royalties, lei 7.990/89 artigo 9. 
 - Valor total de receitas municipais oriundas das indenizações petrolíferas
 - Valor total de petro-rendas passado para os municípios por mil habitantes


Cabe notar que uma determinada variável pode ser independente (explicativa) para um modelo e dependente (explicada) em outro, ou até mesmo compor indicadores tanto do lado esquerdo quanto do lado direito da equação a depender do desenho de pesquisa proposto para responder uma determinada pergunta específica.


#### Coleta dos dados

 Todo o empreendimento descrito foi produzido a partir da coletas de dados de fontes advindas de sites oficiais, especificamente, de cinco sítios virtuais. A saber, o IBGE, o DATASUS, o Tesouro Nacional, IPEA e o TSE. 

 Algumas bases foram acessadas em repositórios de divulgação de dados públicos que disponibilizam bancos de dados já com tratamentos previos que facilitam as análises. O principal deles é o [Basedosdados.org](https://basedosdados.org/), uma extraordinária iniciativa que permitiu solucionar uma série de problemas das bases oficiais, como a integração de fontes distintas e unificação de enconding e formatação, além de permitir o acesso via Google Cloud Plataforma. Neste trabalho valeu-se muito dos dados disponibilizados no datalake gerenciado pela basedosdados.org, principalmente, para os dados do Tesouro Nacional que foram organizados com base no Siconfi.
 
 Nos acervos digitais do IBGE foram colhidos dados que expressam, principalmente, fatores explicativos fundamentais dos municípios brasileiros, quais sejam, o aporte populacional, dados sobre a linha urbana e do transporte, tamanho do eleitorado, entre outros. 

 A partir do DATASUS foram coletados informações referentes a capacidade estrutural dos municípios para prover o Sistema Único de Saúde, principalmente, no repositório [OpenDataSUs](https://opendatasus.saude.gov.br/). Neste sentido, foram realizadas as coletas de informações sobre a assistência à saúde, dos recursos físicos e dos recursos humanos disponibilizados entre outros pontos. Neste acervo digital foram coletados também informações relevantes sobre vacinação e óbitos por COVID-19.

 No banco de dados do Tribunal Superior Eleitoral serão colhidos as informações referentes aos aspectos políticos partidários dos ciclos eleitorais que analisados no trabalho. 


