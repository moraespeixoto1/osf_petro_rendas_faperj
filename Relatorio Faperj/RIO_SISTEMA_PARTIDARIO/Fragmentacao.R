### preparacao dos bancos de candidatos

library(tidyverse)
library(Hmisc)
library(ggthemes)




######  1998 


consulta_cand_1998_RJ <- read_delim("consulta_cand_1998_RJ.txt", 
                                         ";", escape_double = FALSE, col_names = FALSE, 
                                         locale = locale(encoding = "LATIN1"), 
                                         trim_ws = TRUE)

cand_98 <- consulta_cand_1998_RJ %>%  
                   filter(X42 %in% c(1, 5)) %>% 
                   group_by(X9, X10, X18, X19, X20) %>% 
                   summarise(n = n())

cand_98 <- cand_98 %>% rename(V10 = X10,
                              V18 = X18,
                              V19 = X19,
                              V20 = X20,
                              V9 = X9)

##### analise grafica 1998 ####

cand_98 %>% filter(V9 == 6 | V9 == 7) %>% 
             ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n))+
            geom_bar(stat = "identity")+
            facet_wrap(~V10)+
            theme_fivethirtyeight()
            

cand_98 %>% filter(V9 == 6 ) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, , fill = V19 ))+
  geom_bar(stat = "identity")+
  labs(title = "Numero de Deputados Federais eleitos por Partido - RJ - 1998")+
  theme_fivethirtyeight()+
  coord_polar()


####### 2002 

consulta_cand_2002_RJ <- read.table("~/Documentos/ALERJ/consulta_cand_2002_RJ.txt", encoding="LATIN1", sep=";", quote="\"", na.strings="#NULO#")
   
cand_02 <- consulta_cand_2002_RJ %>% 
            filter ( V42 %in% c(1, 5) ) %>% 
            group_by(V9, V10, V18, V19, V20) %>% 
            summarise(n = n())

############# analise grafica 2002


cand_02 %>% filter(V9 == 6 | V9 == 7) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n))+
  geom_bar(stat = "identity")+
  facet_wrap(~V10)+
  theme_fivethirtyeight()+
  labs(title = "Deputados eleitos por Partidos Politicos - RJ - 2002",
       caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")


cand_02 %>% filter(V9 == 6 ) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19 ))+
  geom_bar(stat = "identity")+
  labs(title = "Numero de Deputados Federais eleitos por Partido - RJ - 2002", 
       caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
  theme_fivethirtyeight()+
  coord_polar()

cand_02 %>% filter(V9 == 7 ) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19 ))+
  geom_bar(stat = "identity")+
  labs(title = "Numero de Deputados Estaduais eleitos por Partido - RJ - 2002", 
       caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
  theme_fivethirtyeight()+
  coord_polar()



###### 2006 #####

consulta_cand_2006_RJ <- read.table("~/Documentos/ALERJ/consulta_cand_2006_RJ.txt", encoding="LATIN1", sep=";", quote="\"", na.strings="#NULO#")

cand_06 <- consulta_cand_2006_RJ %>% 
             filter ( V42 %in% c(1, 5) ) %>% 
             group_by(V9, V10, V18, V19, V20) %>% 
             summarise(n = n())


#### analise grafica 2006


cand_06 %>% filter(V9 == 6 ) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19 ))+
  geom_bar(stat = "identity")+
    labs(title = "Numero de Deputados Federais eleitos por Partido - RJ - 2006", 
       caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
  theme_fivethirtyeight()+
  coord_polar()

cand_06 %>% filter(V9 == 7 ) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19 ))+
  geom_bar(stat = "identity")+
      labs(title = "Numero de Deputados Estaduais eleitos por Partido - RJ - 2006", 
       caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
  theme_fivethirtyeight()+
  coord_polar()

######## 2010

consulta_cand_2010_RJ <- read.table("~/Documentos/ALERJ/consulta_cand_2010_RJ.txt", encoding="LATIN1", sep=";", quote="\"", na.strings="#NE#")

cand_10 <-  consulta_cand_2010_RJ %>% 
  filter ( V42 %in% c(1, 5) ) %>% 
  group_by(V9, V10, V18, V19, V20) %>% 
  summarise(n = n())

cand_10 %>% filter(V9 == 6 ) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19 ))+
  geom_bar(stat = "identity")+
  scale_y_continuous(limits = c(0 , 8 ))+
  labs(title = "Numero de Deputados Federais eleitos por Partido - RJ - 2010", 
       caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
  theme_fivethirtyeight()+
  coord_polar()
    
    
  cand_10 %>% filter(V9 == 7 ) %>% 
  ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19 ))+
    scale_y_continuous(limits = c(0 , 12 ))+
  geom_bar(stat = "identity")+
  labs(title = "Numero de Deputados Estaduais eleitos por Partido - RJ - 2010", 
       caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
  theme_fivethirtyeight()+
  coord_polar()

### 2014 ###
  
consulta_cand_2014_RJ <- read.csv2("~/Documentos/ALERJ/consulta_cand_2014_RJ.csv", encoding="LATIN1", comment.char="#", na.strings="#NULO#")
  
    
    cand_14 <- consulta_cand_2014_RJ %>% 
    filter(CD_SIT_TOT_TURNO %in% c("1", "2", "3")) %>% 
      mutate(V19 = as_factor(SG_PARTIDO))   %>% 
      group_by(CD_CARGO, DS_CARGO, NR_PARTIDO, SG_PARTIDO, V19) %>%
      summarise(n = n())
  
  
  cand_14$V18 <- cand_14$NR_PARTIDO   
  cand_14$V9 <- cand_14$CD_CARGO
  cand_14$V10 <- cand_14$DS_CARGO
  
    cand_14 %>% filter(CD_CARGO == 6 ) %>% 
      ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19))+
      geom_bar(stat = "identity")+
      labs(title = "Numero de Deputados Federais eleitos por Partido - RJ - 2014", 
           caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
      theme_fivethirtyeight()+
      coord_polar()
    
    
    cand_14 %>% filter(CD_CARGO == 7) %>% 
      ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19))+
      geom_bar(stat = "identity")+
      labs(title = "Numero de Deputados Estaduais eleitos por Partido - RJ - 2014", 
           caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
      theme_fivethirtyeight()+
      coord_polar()

    
##### 2018
    
    consulta_cand_2018_RJ <- read.csv("~/Documentos/ALERJ/consulta_cand_2018_RJ.csv", encoding="LATIN1", sep=";", comment.char="#", na.strings="#NULO#")
    
    
    cand_18 <- consulta_cand_2018_RJ %>% 
               filter(CD_SIT_TOT_TURNO %in% c("1", "2", "3")) %>% 
               mutate(V19 = as_factor(SG_PARTIDO)) %>% 
               group_by(V19, CD_CARGO, DS_CARGO, NR_PARTIDO, SG_PARTIDO) %>% 
               summarise(n = n())
       
    cand_18$V18 <- cand_18$NR_PARTIDO   
    cand_18$V9 <- cand_18$CD_CARGO
    cand_18$V10 <- cand_18$DS_CARGO
    
    
    
    cand_18 %>% filter(CD_CARGO == 6 ) %>% 
      ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19))+
      geom_bar(stat = "identity")+
      scale_y_continuous(limits = c(0 , 8))+
      labs(title = "Numero de Deputados Federais eleitos por Partido - RJ - 2018", 
           caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
      theme_fivethirtyeight()+
      coord_polar()
    
    
    cand_18 %>% filter(CD_CARGO == 7) %>% 
      ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19))+
      geom_bar(stat = "identity")+
      scale_y_continuous(limits = c(0 , 12 ))+
      labs(title = "Numero de Deputados Estaduais eleitos por Partido - RJ - 2018", 
           caption = "Fonte: Produzido pelo autor com base nos dados oficiais do TSE")+
      theme_fivethirtyeight()+
      coord_polar()

    
    ###### FRAGMENTACAO ###
    
    
    cand_98$ano <- 1998
    cand_02$ano <- 2002    
    cand_06$ano <- 2006    
    cand_10$ano <- 2010    
    cand_14$ano <- 2014
    cand_18$ano <- 2018

   
    #### 1998 #### 
   frag_98 <- cand_98 %>% filter (V9 == 7)
   frag_98$prop_part2 = (frag_98$n/70)^2
   frag_98$prop_part = (frag_98$n/70)
   
   F_98 <- 1 - sum(frag_98$prop_part2)
   
   
   
   
   #### 2002 ####
   
   frag_02 <- cand_02 %>% filter (V9 == 7)
   frag_02$prop_part2 = (frag_02$n/70)^2
   frag_02$prop_part = (frag_02$n/70)
   
   sum(frag_02$prop_part2)
   
   F_02 <- 1 - sum(frag_02$prop_part2)
   
  #### 2006 ####

   frag_06 <- cand_06 %>% filter (V9 == 7)
   frag_06$prop_part2 = (frag_06$n/70)^2
   frag_06$prop_part = (frag_06$n/70)
   
  1 - sum (frag_06$prop_part2)
   
   F_06 <- 1 - sum(frag_06$prop_part2)

   #### 2010 ####
   
   frag_10 <- cand_10 %>% filter (V9 == 7)
   frag_10$prop_part2 = (frag_10$n/70)^2
   frag_10$prop_part = (frag_10$n/70)
   
   sum(frag_10$prop_part2)
   F_10 <- 1 - sum(frag_10$prop_part2)
   F_10 <- 1 - (sum(frag_10$prop_part2))
   
   
   #### 2014 ####
   
   frag_14 <- cand_14 %>% filter (V9 == 7)
   frag_14$prop_part2 = (frag_14$n/70)^2
   frag_14$prop_part = (frag_14$n/70)
   
   F_14 <- 1 - (sum(frag_14$prop_part2))
   
   
   #### 2018 ####
   
   
   frag_18 <- cand_18 %>% filter (V9 == 7)
   frag_18$prop_part2 = (frag_18$n/70)^2
   frag_18$prop_part = (frag_18$n/70)
   
   F_18 <- 1 - (sum(frag_18$prop_part2))
   
   
   
   
   
   ### INDICE DE FRAGMENTACAO ###
   
   
   
   FRAG <- data.frame( ano = c(1998, 2002, 2006, 2010, 2014, 2018),
                       frag_al = c(F_98, F_02, F_06, F_10, F_14, F_18)) 

   save(FRAG, file = "FRAG.Rda")   


   ### INCLUSAO 2022 #### (DADOS INCLUÍDOS PELA PAGINA DA ALERJ ANTES DA LIBERACAO DOS DADOS DO TSE 2022)
   
   
   #### 2022 ####
   
   #dados: site da aler
   
   "https://www.alerj.rj.gov.br/Visualizar/Noticia/54567?AspxAutoDetectCookieSupport=1"
   
   
   ALERJ_2022 <- read_csv("COMPOSICAO_ALERJ_2022.csv")
   
   frag_22 <- ALERJ_2022 %>% 
     group_by(Partido) %>% 
     summarise(n = n(),
               prop = n/70,
               prop2 = prop^2)
   
   F_22 <- 1 - (sum(frag_22$prop2))
   
   
   fragmentacao_2022 <- data.frame(ano = 2022, frag_al = F_22)
   
   load("FRAG.Rda")
   
   FRAG <- rbind(FRAG, fragmentacao_2022)
   
   save(FRAG, file = "FRAG.Rda")
   
   ########
   library(ggthemes)
   
   ggplot(FRAG)+
     aes(x= as_factor(ano), y = frag_al, group = 1)+
     geom_line(size = 1.7, colour = "blue" )+
     theme_fivethirtyeight()+
     labs( title = "Fragmentação da Assembleia Legislativa RJ - ALERJ (1998 - 2022)",
           x = '',
           y = "",
           caption = "Fonte: Produzido pelo autores com base nos dados oficiais do TSE")+
     scale_y_continuous(limits = c(0.8, 1))
   
   
   
   ######## unificacao ####
   
   Cadeiras_RJ <- rbind(cand_98, cand_02, cand_06, cand_10, cand_14, cand_18) 
   
   Cadeiras_RJ <- Cadeiras_RJ %>% select(V9, V10, V18, V19, V20, ano, n) %>% 
                  filter (V9 %in% c(3, 5, 6, 7))
  
   save("Cadeiras_RJ", file = "Cadeiras_RJ.Rda")
      
  load("Cadeiras_RJ.Rda")
   #### Teste grafico 
   
   
   Cadeiras_RJ %>% 
     filter (V9 %in% c(7)) %>% 
     ggplot(aes(x = fct_reorder(V19, n, .desc = TRUE ), y = n, fill = V19))+
     geom_bar(stat = "identity", show.legend = F)+
     facet_wrap(~ ano)+
     coord_polar()
     
   
   ########## ggAnimate 
   
   #######  ALERJ <- Cadeiras_RJ %>% 
   
   filter (V9 %in% c(7)) %>% 
     ggplot(aes(x = fct_reorder(Partido_unico, n, .desc = TRUE ), y = n, fill = Partido_unico))+
     geom_bar(stat = "identity", show.legend = F)+
     scale_y_continuous(limits = c(0,12))+
     coord_polar()+
     labs( title = "Distribuicao das cadeiras da ALERJ por partido 1998-2018", 
           subtitle = '{round(frame_time)}')+
     transition_time(ano) +
     ease_aes('cubic-in-out')
   
   anim_save(ALERJ, filename = "ALERJ.gif")   
     
     