
############################ Preparacao de bancos para calculo de volatilidade 

votacao_partido_munzona_1998_RJ <- read_delim("~/Documentos/ALERJ/votacao_partido_munzona_1998_RJ.txt",
                                              ";", escape_double = FALSE, col_names = FALSE,
                                              locale = locale(encoding = "LATIN1"),
                                              na = "#NULO#", trim_ws = TRUE)

save("votacao_partido_munzona_1998_RJ", file =  "votacao_partido_munzona_1998_RJ.Rda")

load("votacao_partido_munzona_1998.Rda")


VOTOS_98 <- votacao_partido_munzona_1998_RJ %>%
  mutate(X22 = X19 + X20) %>% #votos nos candidatos + votos de legenda
  group_by(X4, X11, X12, X16, X17, X18) %>% # agrupar por cargo e partido
  summarise( votos_partidos_98 = sum(X22)) %>% 
  group_by(X11, X4) %>% # agrupa por cargo e turno
  mutate(votos_validos_98 = sum(votos_partidos_98),
         perc_votos_98 = votos_partidos_98/votos_validos_98 *100,
         n_part = X17)


############################## 2002 ########

votacao_partido_munzona_2002_RJ <- read_delim("~/Documentos/ALERJ/votacao_partido_munzona_2002_RJ.txt", 
                                                  ";", escape_double = FALSE, locale = locale(encoding = "LATIN1"), 
                                                   trim_ws = TRUE, na = "#NULO#" , col_names = FALSE)


save("votacao_partido_munzona_2002_RJ", file =  "votacao_partido_munzona_2002_RJ.Rda")

load("votacao_partido_munzona_2002.Rda")

VOTOS_02 <- votacao_partido_munzona_2002_RJ %>%
  filter(X3 == 2002 ) %>% 
  mutate(X22 = X19 + X20) %>% 
  group_by(X4, X11, X12, X16, X17, X18) %>% # agrupar por cargo e partido
  summarise(votos_partidos_02 = sum(X22)) %>% 
  group_by(X11, X4) %>% # agrupa por cargo
  mutate(votos_validos_02 = sum(votos_partidos_02),
         perc_votos_02 = votos_partidos_02/votos_validos_02 *100,
         n_part = X17)


VOTOS_02 %>%  group_by(X12) %>% 
  summarise(n = n(),
            t.teste = sum(perc_votos_02) )



##################### 2006 

votacao_partido_munzona_2006_RJ <- read_delim("~/Documentos/ALERJ/votacao_partido_munzona_2006_RJ.txt", 
                                              ";", escape_double = FALSE, col_names = FALSE, na = "#NULO#",
                                              locale = locale(encoding = "LATIN1"), 
                                              trim_ws = TRUE)
save("votacao_partido_munzona_2006_RJ", file =  "votacao_partido_munzona_2006_RJ.Rda")

load("votacao_partido_munzona_2006.Rda")

VOTOS_06 <- votacao_partido_munzona_2006_RJ %>% 
           mutate(X22 = X19 + X20) %>% 
           group_by(X4, X11, X12, X16, X17, X18) %>% 
           summarise(votos_partidos_06 = sum(X22)) %>% 
           group_by(X11, X4) %>% # agrupa por cargo
           mutate(votos_validos_06 = sum(votos_partidos_06),
         perc_votos_06 = votos_partidos_06/votos_validos_06 *100,
         n_part = X17)
        

VOTOS_06 %>%  group_by(X12, X4) %>% 
  summarise(n = n(),
            t.teste = sum(perc_votos_06) )


################################# 2010 

votacao_partido_munzona_2010_RJ <- read_delim("~/Documentos/ALERJ/votacao_partido_munzona_2010_RJ.txt", 
                                              ";", escape_double = FALSE, col_names = FALSE, 
                                              locale = locale(encoding = "LATIN1"), 
                                              na = "#NULO#", trim_ws = TRUE)

save("votacao_partido_munzona_2010_RJ", file =  "votacao_partido_munzona_2010_RJ.Rda")

load("votacao_partido_munzona_2010.Rda")

VOTOS_10 <- votacao_partido_munzona_2010_RJ %>% 
  mutate(X22 = X19 + X20) %>% 
  group_by(X4, X11, X12, X16, X17, X18) %>% 
  summarise(votos_partidos_10 = sum(X22)) %>% 
  group_by(X11, X4) %>% # agrupa por cargo e turno
  mutate(votos_validos_10 = sum(votos_partidos_10),
         perc_votos_10 = votos_partidos_10/votos_validos_10 *100,
         n_part = X17)


VOTOS_10 %>%  group_by(X12, X4) %>% 
  summarise(n = n(),
            t.teste = sum(perc_votos_10) )


######################################   2014

library(readr)
votacao_partido_munzona_2014_RJ <- read_delim("~/Documentos/ALERJ/votacao_partido_munzona_2014_RJ.csv", 
                                              ";", escape_double = FALSE, locale = locale(encoding = "LATIN1"), 
                                              trim_ws = TRUE)

save("votacao_partido_munzona_2014_RJ", file =  "votacao_partido_munzona_2014_RJ.Rda")

load("votacao_partido_munzona_2014.Rda")

VOTOS_14 <- votacao_partido_munzona_2014_RJ %>% 
            mutate(X22 = QT_VOTOS_NOMINAIS + QT_VOTOS_LEGENDA) %>% 
            group_by(NR_TURNO, CD_CARGO, DS_CARGO, NR_PARTIDO, SG_PARTIDO, NM_PARTIDO) %>% 
            summarise(votos_partidos_14 = sum(X22) ) %>% 
            group_by(CD_CARGO, DS_CARGO, NR_TURNO) %>% 
            mutate(votos_validos_14 = sum(votos_partidos_14),
                   perc_votos_14 = votos_partidos_14/votos_validos_14 *100,
                   n_part = NR_PARTIDO)
            
  
  VOTOS_14 %>% group_by(DS_CARGO, NR_TURNO) %>% 
              summarise(n = n(),
                        t.test= sum(perc_votos_14))

  ################################# 2018
  
  library(readr)
  votacao_partido_munzona_2018_RJ <- read_delim("~/Documentos/ALERJ/votacao_partido_munzona_2018_RJ.csv", 
                                                ";", escape_double = FALSE, locale = locale(encoding = "LATIN1"), 
                                                trim_ws = TRUE)
  
  save("votacao_partido_munzona_2018_RJ", file =  "votacao_partido_munzona_2018_RJ.Rda")
  
  load("votacao_partido_munzona_2018.Rda")
  
  VOTOS_18 <- votacao_partido_munzona_2018_RJ %>% 
    mutate(X22 = QT_VOTOS_NOMINAIS + QT_VOTOS_LEGENDA) %>% 
    group_by(NR_TURNO, CD_CARGO, DS_CARGO, NR_PARTIDO, SG_PARTIDO, NM_PARTIDO) %>% 
    summarise(votos_partidos_18 = sum(X22) ) %>% 
    group_by(CD_CARGO, DS_CARGO, NR_TURNO) %>% 
    mutate(votos_validos_18 = sum(votos_partidos_18),
           perc_votos_18 = votos_partidos_18/votos_validos_18 *100,
           n_part = NR_PARTIDO)
  
  VOTOS_18 %>% group_by(DS_CARGO, NR_TURNO) %>% 
    summarise(n = n(),
              t.test= sum(perc_votos_18))
  
  
  ########################  unificacao dos bancos por partido
  
  P_V_18 <- VOTOS_18 %>% 
    filter(NR_TURNO == 1) %>% 
    select(CD_CARGO, NR_PARTIDO, SG_PARTIDO, perc_votos_18, n_part) %>% 
    mutate(X11 = CD_CARGO) %>% 
    rename(SG_PARTIDO_18 = SG_PARTIDO)
  
  P_V_14 <- VOTOS_14 %>% 
    filter(NR_TURNO == 1) %>% 
    rename(SG_PARTIDO_14 = SG_PARTIDO) %>% 
    select(CD_CARGO, NR_PARTIDO, SG_PARTIDO_14, perc_votos_14, n_part) %>% 
    mutate(X11 = CD_CARGO)

  P_V_10 <- VOTOS_10 %>% 
    filter(X4 == 1) %>% 
    select(X11, X16, perc_votos_10, n_part ) 

  P_V_06 <- VOTOS_06 %>% 
    filter(X4 == 1) %>% 
    select(X11, X16, perc_votos_06, n_part ) 

  P_V_02 <- VOTOS_02 %>% 
    filter(X4 == 1) %>% 
    select(X11, X16, perc_votos_02, n_part ) 

  P_V_98 <- VOTOS_98 %>% 
    filter(X4 == 1) %>% 
    select(X11, X16, perc_votos_98, n_part ) 
    

  ######  O MERGE 
  
VOTOS_18_14 <- merge(P_V_18, P_V_14, by = c("X11", "n_part"), all.x = T, all.y = T)  
VOTOS_18_14_10 <- merge(VOTOS_18_14, P_V_10, by = c("X11", "n_part"), all.x = T, all.y = T)
VOTOS_18_14_10_06 <- merge(VOTOS_18_14_10, P_V_06, by = c("X11", "n_part"), all.x = T, all.y = T)                      
VOTOS_18_14_10_06_02 <- merge(VOTOS_18_14_10_06, P_V_02, by = c("X11", "n_part"), all.x = T, all.y = T) %>% 
  select(-X4.x, -X4.y, -X16.x, -X16.y)
VOTOS_18_14_10_06_02_98 <- merge(VOTOS_18_14_10_06_02, P_V_98, by = c("X11", "n_part"), all.x = T, all.y = T) 

volatilidade <- VOTOS_18_14_10_06_02_98 %>% 
  mutate_all(replace_na, 0) %>% 
  mutate(dif_18_14 = abs(perc_votos_18 - perc_votos_14),
         dif_14_10 = abs(perc_votos_14 - perc_votos_10),
         dif_10_06 = abs(perc_votos_10 - perc_votos_06),
         dif_06_02 = abs(perc_votos_06 - perc_votos_02),
         dif_02_98 = abs(perc_votos_02 - perc_votos_98)) %>%
    group_by(X11) %>%
  summarise(V_18_14 = (sum(dif_18_14))/2,
            V_14_10 = (sum(dif_14_10))/2,
            V_10_06 = (sum(dif_10_06))/2,
            V_06_02 = (sum(dif_06_02))/2,
            V_02_98 = (sum(dif_02_98))/2 ) %>% 
  mutate(Cargo = as_factor(X11),
         Cargo = recode(Cargo, '1' = "Presidente", '3' = "Governador", '5' = "Senador", '6' = "Dep_Federal", '7' = "Dep_Estadual"))

save("volatilidade", file = "volatilidade.Rda")

long_volatilidade <- volatilidade %>% 
                    gather(anos, value = long_volatilidade, - Cargo, -X11)  

save("long_volatilidade", file = "long_volatilidade.Rda")

wide_volatilidade <- long_volatilidade %>% 
                           select( -1 ) %>% 
                           spread(Cargo, long_volatilidade)

wide_volatilidade$ano <- c(2002, 2006, 2010, 2014, 2018)

save("wide_volatilidade", file = "wide_volatilidade.Rda")
