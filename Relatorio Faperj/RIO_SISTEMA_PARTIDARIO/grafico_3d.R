devtools::install_bitbucket("derpylz/babyplots.git")
install.packages("plot3D")

library(tidyverse)
library(plot3D)
library(babyplots)


load("")

plot3d(z= comp_1$p_brancos, y = comp_1$p_nulos, x = comp_1$IDEB.INI.PUB.15,
       col = comp_1$REGIAO_Num,
       zlab = "Brancos",
       xlab = "IDEB",
       ylab = "Nulos",
       radius = .4,
       type = "s",
       main = "Votos brancos, nulos e IDEB por Regiao nas eleicoes municipais de 2016")



play3d( spin3d( axis = c(0, 0, 1), rpm = 6), duration = 20 )


movie3d(
  movie="Comparecimento_pop_validos", 
  spin3d( axis = c(0, 0, 1), rpm = 6),
  dir = "E:/PROJETOS_GIT",
  duration = 20, 
  type = "gif", 
  clean = TRUE
)


